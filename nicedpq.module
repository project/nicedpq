<?php

use Drupal\nicedpq as m;

/**
 * Nice-print a select query.
 *
 * @param SelectQuery $q
 *   The query object.
 * @param boolean $return
 *   If TRUE, the function will return the string.
 *   If FALSE, it will dpm() it.
 */
function nicedpq($q, $return = FALSE) {
  if ($q instanceof EntityFieldQuery) {
    if ($return) {
      throw new \Exception("nicedpq() on EntityFieldQuery cannot be used with the $return option.");
    }
    $q->addTag('nicedpq');
    return;
  }

  // Make sure the query gets compiled.
  $q->__toString();

  $text = '';
  $s = new m\IndentedText($text);
  $p = new m\QueryPrinter();
  $p->printSelectQuery($s, $q);

  if ($return) {
    return $text;
  }
  // Use Devel's dpm() for the final output.
  dpm($text);
}

/**
 * Implements hook_query_TAG_alter().
 */
function nicedpq_query_nicedpq_alter($q) {
  nicedpq($q);
}
